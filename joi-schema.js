const Joi = require('joi');

exports.DEFAULT_REQUEST_OPTIONS = {
    abortEarly: false, 
    allowUnknown: true,
    stripUnknown: false
};

exports.REFISTER_DRONE = Joi.object({
    serialNumber: Joi.string().max(100).required(),
    model: Joi.string().valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight').required(),
    weightLimit: Joi.number().max(500).required(),
    batteryCapacity: Joi.number().max(1).required(),
    state: Joi.string().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING').required()
});

exports.UPDATE_DRONE = Joi.object({
    serialNumber: Joi.string().max(100),
    model: Joi.string().valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'),
    weightLimit: Joi.number().max(500),
    batteryCapacity: Joi.number().max(1),
    state: Joi.string().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING')
});

exports.MEDICATIIN_ITEM = Joi.object({
    medicationId: Joi.number().required(),
    quantity: Joi.number().min(1).required()
});

exports.LOAD_DRONE= Joi.object({
    droneId: Joi.number().required(),
    medications: Joi.array().min(1).items(this.MEDICATIIN_ITEM).unique('medicationId').required()
});


exports.MEDICATION_MODEL = Joi.object({
    name: Joi.string().pattern(new RegExp('^[A-Za-z0-9_-]*$')).required(),
    weight: Joi.number().required(),
    code: Joi.string().pattern(new RegExp('^[A-Z0-9_]')).required()
});
