var express = require('express');
var router = express.Router();
const droneController = require('../controllers/droneController')
const medicationController = require('../controllers/medicationController')

/**Drone */
router.get('/drone', droneController.getDrones);
router.get('/drone/:id', droneController.getDroneById);
router.get('/drone/:id/baterry', droneController.getDroneBatteryLvl);
router.post('/drone/register', droneController.registerDrone);
router.post('/drone/load', droneController.loadDrone);
router.get('/drone/load/:id', droneController.getLoadedItems);
router.get('/drone/check/available', droneController.availableDrones);
router.patch('/drone/:id', droneController.updateDroneDetails);


/**Medication */
router.get('/medication', medicationController.getMedications);
router.get('/medication/:id', medicationController.getMedicationById);
router.post('/medication', medicationController.createMedication);
//router.patch('/medication', droneController.getDroneById);


module.exports = router;
