# Drones Project
Musala Test project

This project was generated with [ExpressJs Generator](https://expressjs.com/en/starter/generator.html).

## Database

SqlLite - No setup required

## Development server

Node Version 8.5.1 and above
`npm -v`

Node Version 17.6.0 and above
`node -v`

Install all the packages
`npm install`

Generate database and seed test data
`npm run init-data`

Run the project
`npm start` default port will be 3000


## Build

Run `npm run build`. The dist folder will be generated.

## Postman Collection

`Musala.postman_collection.json`

