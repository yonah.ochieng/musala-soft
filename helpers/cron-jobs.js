const cron = require('node-cron');
const helpers = require('./helpers')

exports.initCronJobs = async()=>{

    try {
        //Every one minute
        var task = cron.schedule('* * * * *', async() =>  {
    
            helpers.logBatteryStatus()
    
        }, 
        {
          scheduled: true
        });
    
        task.start();
        
    } catch (error) {
        console.error(error)
    }
}