const db = require("../models");
const { Op, Sequelize } = require("sequelize");

exports.validDroneId = async(id)=>{
   var count = await db.Drone.count({
        where:{
            id:id
        }
    });

    return count>0;
}

exports.validMedicationId = async(id)=>{
    var count = await db.Medication.count({
         where:{
             id:id
         }
     });
 
     return count>0;
}

exports.validMedication =(medication) => new Promise(async(resolved, reject)=>{
    const id = medication.medicationId;

    if(!await this.validMedicationId(id)){
        //throw new Error(`Invalid Medication id ${id}`);
        reject(`Invalid Medication id ${id}`)
    }
    resolved()
});

exports.getDrones = async()=>{
    return await db.Drone.findAll();
}

exports.getDroneDetails = async(id)=>{
    return await db.Drone.findOne({
         where:{
             id:id
         }
     });
 }

 exports.getDMedicationDetails = async(id)=>{
     return await db.Medication.findOne({
          where:{
              id:id
          }
      });
}

exports.droneCanLoad = async(id, medications)=>{
    var isvalid = true;
    var errors = [] 

    if(!await this.validDroneId(id)){
        isvalid = false
        errors.push({
            "error":`Inavalid Drone Id ${id}`
        })
    }    

    const droneDetails = await this.getDroneDetails(id);

    if(droneDetails.state != 'IDLE')
    {
        isvalid = false
        errors.push({
            "error":`Drone With the status ${droneDetails.state} cannot be loaded`
        })
    }
    
    if(droneDetails.batteryCapacity <= 0.25){
        isvalid = false
        errors.push({
            "error":`Drone With Battery capacity ${droneDetails.batteryCapacity} cannot be loaded`
        })
    }
    // const promises = [];

    // medications.forEach((medication) => {
    //     promises.push(this.validMedication(medication))
    // });
    // await Promise.all(promises);

    const medicationsDetails = await db.Medication.findAll({
        where:{
            id:{
                [Op.in]: medications.map((x)=>{
                    return x.medicationId
                })
            }
        },
        raw:true
    });

    var totalWeight = 0;
    medications.map((x)=>{
        totalWeight+= (x.quantity * medicationsDetails.find(y => y.id == x.medicationId).weight)
    })

    if(totalWeight > droneDetails.weightLimit){
        isvalid = false
        errors.push({
            "error":`The toatal weight ${totalWeight} exceed the drone weight of ${droneDetails.weightLimit}`
        })
    }

    return {
        isvalid:isvalid,
        errors: errors
    }
}

exports.updateDroneStatus = async(id, state) =>{
    console.log(id)

    const droneDetails = await this.getDroneDetails(id);
    if(droneDetails){

        if(droneDetails.batteryCapacity < 0.25 && state == 'LOADING'){
            throw new Error(`Cannot be loaded with batteryCapacity below 25%`)
        }
    
        await db.Drone.update({state:state},{
            where:{
                id:id
            }
        })

    }
}

exports.loadDrone = async(id, medications)=>{

    await db.Dispatch.destroy({ where: { droneId: id } });

    const payload = medications.map(x => {
        return {...x, ...{droneId: id}}
    });

    results = await db.Dispatch.bulkCreate(payload, {
        gnoreDuplicates: true,
    })

    return results
}

exports.logBatteryStatus = async() =>{
    console.info('Logging Battey Status');

    var drones = await this.getDrones();

    drones.forEach(async(drone) => {
        await db.BatterLevelHist.create({
            droneId: drone.id,
            batteryCapacity: drone.batteryCapacity
        })
    });
}



/**
 * ==================================Medications==============
 */

exports.getMedications = async()=>{
    return await db.Medication.findAll();
}

exports.getMedicationById = async(id)=>{

    return await db.Medication.findOne({
        where:{
            id:id
        }
    });
 }