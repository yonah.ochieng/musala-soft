const db = require("../models");
const { Op, Sequelize, HasMany } = require("sequelize");
const joi = require("../joi-schema")
const helpers = require('../helpers/helpers')

exports.getDrones = async (req, res) => {
    res.send(await helpers.getDrones());
}

exports.getDroneById = async (req, res) => {
    res.send(await helpers.getDroneDetails(req.params.id));
}

exports.getDroneBatteryLvl = async (req, res) => {

    const details = await helpers.getDroneDetails(req.params.id);


    if (details) {

        res.send({
            "batteryCapacity": details.batteryCapacity
        })
    }
    else {
        res.status(401)
    }
}

exports.registerDrone = async (req, res) => {
    const schema = joi.REFISTER_DRONE

    const {
        error,
        value
    } = schema.validate(req.body, joi.DEFAULT_REQUEST_OPTIONS);

    if (error) {
        return res.status(400).send(error.details);
    }

    try {

        console.log(req.body)
        var data = await db.Drone.create(req.body)
        res.send(data)
    }
    catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

exports.updateDroneDetails = async(req, res) =>{
    const id = req.params.id;
    var data = req.body;
    delete data.id

    const schema = joi.UPDATE_DRONE

    const {
        error,
        value
    } = schema.validate(req.body, joi.DEFAULT_REQUEST_OPTIONS);

    if (error) {
        return res.status(400).send(error.details);
    }

    try {
        await db.Drone.update(data, {
            where:{
                id:id
            },
            raw:true
        })
    
        const result = await helpers.getDroneDetails(id);
    
        console.log(result)
        res.send(result);
        
    } catch (error) {
        res.status(500).send(error);
    }

}

exports.availableDrones = async (req, res) => {
    const result = await db.Drone.findAll({
        where: {
            state: ['IDLE'],
            batteryCapacity: {
                [Op.gt]: 0.25
            }
        }
    });

    res.send(result)
}

exports.loadDrone = async (req, res) => {
    const schema = joi.LOAD_DRONE

    const {
        error,
        value
    } = schema.validate(req.body, joi.DEFAULT_REQUEST_OPTIONS);

    if (error) {
        return res.status(400).send(error.details);
    }

    try {
        const validator = await helpers.droneCanLoad(req.body.droneId, req.body.medications)

        console.log(validator)
        console.log(validator)

        if (!validator.isvalid) {
            return res.status(400).send(validator.errors)
        }

        await helpers.updateDroneStatus(req.body.droneId, 'LOADING')

        const result = await helpers.loadDrone(req.body.droneId, req.body.medications)

        await helpers.updateDroneStatus(req.body.droneId, 'LOADED')

        res.send(result)

    } catch (error) {
        console.error(error)
        res.status(400).send(error)
    }
}

exports.getLoadedItems = async (req, res) => {
    const id = req.params.id;

    const dispatches = await db.Dispatch.findAll({
        where: {
            droneId: id
        },
        include: [
            {
                model: db.Medication,
                require: true
            }
        ],
        //raw:true
    });

    console.log()

    res.send(dispatches)
}