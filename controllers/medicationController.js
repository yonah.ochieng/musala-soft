const db = require("../models");
const { Op, Sequelize, HasMany } = require("sequelize");
const joi = require("../joi-schema")
const helpers = require('../helpers/helpers')

exports.getMedications = async (req, res) => {
    res.send(await helpers.getMedications());
}

exports.getMedicationById = async (req, res) => {
    res.send(await helpers.getMedicationById(req.params.id));
}

exports.createMedication= async (req, res) => {

    const schema = joi.MEDICATION_MODEL

    const {
        error,
        value
    } = schema.validate(req.body, joi.DEFAULT_REQUEST_OPTIONS);

    if (error) {
        return res.status(400).send(error.details);
    }


    var data = await db.Medication.create(req.body)
    res.send(data)
}