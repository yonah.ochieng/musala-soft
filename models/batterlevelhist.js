'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BatterLevelHist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BatterLevelHist.init({
    droneId: DataTypes.INTEGER,
    batteryCapacity: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'BatterLevelHist',
  });
  return BatterLevelHist;
};