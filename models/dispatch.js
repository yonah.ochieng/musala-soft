'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Dispatch extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
     // this.belongsToMany(models.Drone)
      //this.belongsToMany(models.Medication)
    }
  }
  Dispatch.init({
    droneId: DataTypes.INTEGER,
    medicationId: DataTypes.INTEGER,
    quantity: DataTypes.FLOAT,
  }, {
    sequelize,
    modelName: 'Dispatch',
  });
  return Dispatch;
};