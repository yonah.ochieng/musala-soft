'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Drones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      serialNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        unique:true,
        validate:{
          len: [0,100]
        }
      },
      model: {
        type: Sequelize.STRING
      },
      weightLimit: {
        type: Sequelize.FLOAT
      },
      batteryCapacity: {
        type: Sequelize.FLOAT
      },
      state: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Drones');
  }
};