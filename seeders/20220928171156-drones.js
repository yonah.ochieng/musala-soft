'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert('Drones', [
      {
        serialNumber: 'SRL-123456-1',
        model: 'Lightweight',
        weightLimit:50,
        batteryCapacity:0.6,
        state:'IDLE',
        createdAt: new Date(),
        updatedAt: new Date()
       },
      {
        serialNumber: 'SRL-123456-2',
        model: 'Middleweight',
        weightLimit:100,
        batteryCapacity:0.7,
        state:'IDLE',
        createdAt: new Date(),
        updatedAt: new Date()
       },
       {
         serialNumber: 'SRL-123456-3',
         model: 'Middleweight',
         weightLimit:200,
         batteryCapacity:0.7,
         state:'IDLE',
         createdAt: new Date(),
         updatedAt: new Date()
        },
        {
          serialNumber: 'SRL-123456-4',
          model: 'Cruiserweight',
          weightLimit:250,
          batteryCapacity:0.2,
          state:'IDLE',
          createdAt: new Date(),
          updatedAt: new Date()
         },
         {
           serialNumber: 'SRL-123456-5',
           model: 'Cruiserweight',
           weightLimit:300,
           batteryCapacity:0.3,
           state:'IDLE',
           createdAt: new Date(),
           updatedAt: new Date()
          },
          {
            serialNumber: 'SRL-123456-6',
            model: 'Lightweight',
            weightLimit:50,
            batteryCapacity:0.6,
            state:'IDLE',
            createdAt: new Date(),
            updatedAt: new Date()
           },
          {
            serialNumber: 'SRL-123456-7',
            model: 'Middleweight',
            weightLimit:100,
            batteryCapacity:0.7,
            state:'IDLE',
            createdAt: new Date(),
            updatedAt: new Date()
           },
           {
             serialNumber: 'SRL-123456-8',
             model: 'Middleweight',
             weightLimit:200,
             batteryCapacity:0.7,
             state:'IDLE',
             createdAt: new Date(),
             updatedAt: new Date()
            },
            {
              serialNumber: 'SRL-123456-9',
              model: 'Cruiserweight',
              weightLimit:250,
              batteryCapacity:0.2,
              state:'IDLE',
              createdAt: new Date(),
              updatedAt: new Date()
             },
             {
               serialNumber: 'SRL-123456-10',
               model: 'Cruiserweight',
               weightLimit:300,
               batteryCapacity:0.3,
               state:'IDLE',
               createdAt: new Date(),
               updatedAt: new Date()
              }
      ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Drones', null, {});
  }
};
