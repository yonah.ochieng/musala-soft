'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Medications', [
      {
        name: 'Ampicillin',
        weight: 50,
        code:'MED-001',
        image:'',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Augmentin',
        weight: 20,
        code:'MED-002',
        image:'',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Medications', null, {});
  }
};
